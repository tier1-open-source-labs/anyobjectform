/*
 * Name:    DynamicRecordPageTests
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */

@isTest (seeAllData = false)
private with sharing class DynamicRecordPageTests 
{
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();
    private static Account acct;
    
    static testMethod void testParse() {
		String json= '{"controllerValues":{},"defaultValue":null,"eTag":"93b7827114fa9bbce28096d56500d977","url":"/services/data/v47.0/ui-api/object-info/Contact/picklist-values/012000000000000AAA/LeadSource","values":[{"attributes":null,"label":"Web","validFor":[],"value":"Web"},{"attributes":null,"label":"Phone Inquiry","validFor":[],"value":"Phone Inquiry"},{"attributes":null,"label":"Partner Referral","validFor":[],"value":"Partner Referral"},{"attributes":null,"label":"Purchased List","validFor":[],"value":"Purchased List"},{"attributes":null,"label":"Other","validFor":[],"value":"Other"}]}';
		RestPicklistReturn obj = RestPicklistReturn.parse(json);
    }
    
    static testMethod void dynamicPageTestViewLowerCase() 
    {
        setupData();    
        insert acct;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockRestPicklistReturn());
        String info = DynamicRecordPageController.getConfig(acct.Id, 'test?mode=view&object=account&parentid=' + acct.Id, UserInfo.getUserId());

        DynamicRecordPageController.PageDetail detail = (DynamicRecordPageController.PageDetail) JSON.deserialize(info, DynamicRecordPageController.PageDetail.class);

        Test.stopTest();
    }
    
    static testMethod void dynamicPageTestView() 
    {
        setupData();    

        insert acct;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockRestPicklistReturn());
        String info = DynamicRecordPageController.getConfig(acct.Id, 'test?mode=View&object=Account&Id=' + acct.Id, UserInfo.getUserId());

        DynamicRecordPageController.PageDetail detail = (DynamicRecordPageController.PageDetail) JSON.deserialize(info, DynamicRecordPageController.PageDetail.class);
        detail.relatedLists.sort();

        Test.stopTest();
    }

    static testMethod void dynamicPageTestCreate() 
    {
        setupData();    

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockRestPicklistReturn());
        String info = DynamicRecordPageController.getConfig(null, 'test?mode=View&object=Account', UserInfo.getUserId());

        Test.stopTest();
   }

    static testMethod void dynamicPageTestSave() 
    {
        setupData();    
        insert acct;

        Test.startTest();

        String info = DynamicRecordPageController.saveInfo('{"Name":"Paper Street Soap Company","AnnualRevenue":"123456","T1C_Base__Telephony_Last_Modified_Date__c":1558414800000,"T1C_Base__Last_User_Interaction_Date__c":1558411200000,"sobjectType":"Account"}', 'Account', 'true');

        String saveInfos = JSON.serialize(new list<SObject>{new Contact(AccountId=acct.Id, FirstName='Tyler', LastName='Durden', Email='tdurden@paperstreetsoapcompany.com')});

        System.debug(saveInfos);

        String infos = DynamicRecordPageController.saveRelatedInfo(saveInfos, 'Contact', 'true');

        List<SObject> objsList = new list<SObject>{(SObject)acct};

        infos = DynamicRecordPageController.deleteRelatedInfo(JSON.serialize(objsList));

        Test.stopTest();
    }

    static testMethod void dynamicPageTestUpdate() 
    {
        setupData();    

        Test.startTest();
        insert acct;
        String info = DynamicRecordPageController.saveInfo('{"Id":"' + acct.Id + '","Name":"Paper Street Soap Company","AnnualRevenue":"123456","T1C_Base__Telephony_Last_Modified_Date__c":1558414800000,"T1C_Base__Last_User_Interaction_Date__c":1558411200000,"sobjectType":"Account"}', 'Account', 'true');
        DynamicRecordPageController.SaveRes saveResult = (DynamicRecordPageController.SaveRes) JSON.deserialize(info, DynamicRecordPageController.SaveRes.class);
        
        System.assert(saveResult.returnId != null);

        Test.stopTest();
    }

    private static void setupData()
    {
        dataFactory.createCBSAFR();

        T1C_FR__Feature__c ACEExtensions = new T1C_FR__Feature__c(Name = 'Extensions', T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c ACEExtensionsDynamicPage = new T1C_FR__Feature__c(Name = 'DynamicPage', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccount = new T1C_FR__Feature__c(Name = 'Account', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account');

        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedLists = new T1C_FR__Feature__c(Name = 'RelatedLists', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContacts = new T1C_FR__Feature__c(Name = 'Contacts', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfig = new T1C_FR__Feature__c(Name = 'DefaultItemConfig', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.DefaultItemConfig');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDate = new T1C_FR__Feature__c(Name = 'Date', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.DefaultItemConfig.Date');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB = new T1C_FR__Feature__c(Name = 'LOB', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.DefaultItemConfig.LOB');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDateTime = new T1C_FR__Feature__c(Name = 'DateTime', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.DefaultItemConfig.DateTime');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsFields = new T1C_FR__Feature__c(Name = 'FieldsName', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.Fields');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsName = new T1C_FR__Feature__c(Name = 'Name', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.Fields.Name');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsTelLastModDateTime = new T1C_FR__Feature__c(Name = 'TelLastModDateTime', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.Fields.TelLastModDateTime');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountRelatedListsContactsUCIDate = new T1C_FR__Feature__c(Name = 'UCIDate', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.RelatedLists.Contacts.Fields.UCIDate');

        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountSectionConfig = new T1C_FR__Feature__c(Name = 'SectionConfig', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.SectionConfig');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountSectionConfigSection1 = new T1C_FR__Feature__c(Name = 'Section1', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.SectionConfig.Section1');

        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountSectionVisibilities = new T1C_FR__Feature__c(Name = 'SectionVisibilities', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.SectionVisibilities');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountSectionVisibilitiesSection1 = new T1C_FR__Feature__c(Name = 'Section1', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.SectionVisibilities.Section1');

        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountButtons = new T1C_FR__Feature__c(Name = 'Buttons', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.Buttons');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountButtonsBack = new T1C_FR__Feature__c(Name = 'Back', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.Buttons.Back');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountButtonsForward = new T1C_FR__Feature__c(Name = 'Forward', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.Buttons.Forward');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountLookups = new T1C_FR__Feature__c(Name = 'Lookups', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.Lookups');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountLookupsAccount = new T1C_FR__Feature__c(Name = 'Account', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.Lookups.Account');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountView = new T1C_FR__Feature__c(Name = 'View', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewAnnualRevenue = new T1C_FR__Feature__c(Name = 'AnnualRevenue', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.AnnualRevenue');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewDateTime = new T1C_FR__Feature__c(Name = 'DateTime', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.DateTime');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewTier = new T1C_FR__Feature__c(Name = 'Tier', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.Tier');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewName = new T1C_FR__Feature__c(Name = 'Name', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.Name');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewParentId = new T1C_FR__Feature__c(Name = 'ParentId', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.ParentId');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewCreatedBy = new T1C_FR__Feature__c(Name = 'CreatedBy', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.CreatedBy');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewInactive = new T1C_FR__Feature__c(Name = 'Inactive', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.Inactive');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewCreatedById = new T1C_FR__Feature__c(Name = 'CreatedById', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.CreatedById');
        T1C_FR__Feature__c ACEExtensionsDynamicPageAccountViewFake = new T1C_FR__Feature__c(Name = 'Fake', T1C_FR__Name__c = 'ACE.Extensions.DynamicPage.Account.View.Fake');
        
        insert new list<T1C_FR__Feature__c>{ACEExtensions,ACEExtensionsDynamicPage,ACEExtensionsDynamicPageAccount,ACEExtensionsDynamicPageAccountRelatedLists,
                                            ACEExtensionsDynamicPageAccountButtons,ACEExtensionsDynamicPageAccountSectionConfig,ACEExtensionsDynamicPageAccountSectionConfigSection1,
                                            ACEExtensionsDynamicPageAccountSectionVisibilities,ACEExtensionsDynamicPageAccountSectionVisibilitiesSection1,
                                            ACEExtensionsDynamicPageAccountRelatedListsContacts,ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfig,
                                            ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDate,ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB,
                                            ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDateTime,ACEExtensionsDynamicPageAccountRelatedListsContactsFields,
                                            ACEExtensionsDynamicPageAccountRelatedListsContactsName,ACEExtensionsDynamicPageAccountRelatedListsContactsTelLastModDateTime,ACEExtensionsDynamicPageAccountRelatedListsContactsUCIDate,
                                            ACEExtensionsDynamicPageAccountButtonsBack,ACEExtensionsDynamicPageAccountButtonsForward,ACEExtensionsDynamicPageAccountLookups,
                                            ACEExtensionsDynamicPageAccountLookupsAccount,ACEExtensionsDynamicPageAccountView,ACEExtensionsDynamicPageAccountViewAnnualRevenue,
                                            ACEExtensionsDynamicPageAccountViewDateTime,ACEExtensionsDynamicPageAccountViewTier,ACEExtensionsDynamicPageAccountViewName,
                                            ACEExtensionsDynamicPageAccountViewParentId,ACEExtensionsDynamicPageAccountViewCreatedBy,
                                            ACEExtensionsDynamicPageAccountViewInactive,ACEExtensionsDynamicPageAccountViewCreatedById,ACEExtensionsDynamicPageAccountViewFake};

        insert new list<T1C_FR__Attribute__c>
		{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'ChildRelationName', T1C_FR__Value__c = 'Contacts'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'Label', T1C_FR__Value__c = 'Contacts'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'ParentLookup', T1C_FR__Value__c = 'AccountId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'Section', T1C_FR__Value__c = '9'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContacts.Id, Name = 'SObjectName', T1C_FR__Value__c = 'Contact'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDate.Id, Name = 'FieldType', T1C_FR__Value__c = 'Date'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDate.Id, Name = 'NewRecordDefaultFieldName', T1C_FR__Value__c = 'T1C_Base__Last_User_Interaction_Date__c'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDateTime.Id, Name = 'FieldType', T1C_FR__Value__c = 'DateTime'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigDateTime.Id, Name = 'NewRecordDefaultFieldName', T1C_FR__Value__c = 'T1C_Base__Telephony_Last_Modified_Date__c'),
            //10
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB.Id, Name = 'FieldType', T1C_FR__Value__c = 'String'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB.Id, Name = 'NewRecordDefaultFieldName', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB.Id, Name = 'QueryObjectName', T1C_FR__Value__c = 'T1C_Base__Employee__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB.Id, Name = 'QueryObjectField', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsDefaultItemConfigLOB.Id, Name = 'QueryObjectFilterClause', T1C_FR__Value__c = 'T1C_Base__User__c = :userId'),

            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsName.Id, Name = 'DataField', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsName.Id, Name = 'Order', T1C_FR__Value__c = '1'),

            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsTelLastModDateTime.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Telephony_Last_Modified_Date__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsTelLastModDateTime.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsTelLastModDateTime.Id, Name = 'Handler', T1C_FR__Value__c = 'DateTime'),

            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsUCIDate.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Last_User_Interaction_Date__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsUCIDate.Id, Name = 'Order', T1C_FR__Value__c = '3'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountRelatedListsContactsUCIDate.Id, Name = 'Handler', T1C_FR__Value__c = 'Date'),

            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountSectionConfigSection1.Id, Name = 'Section', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountSectionConfigSection1.Id, Name = 'AdditionalClassNames', T1C_FR__Value__c = 'displayFlex'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountSectionVisibilitiesSection1.Id, Name = 'SectionId', T1C_FR__Value__c = 'section5'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountSectionVisibilitiesSection1.Id, Name = 'CompareFieldName', T1C_FR__Value__c = 'T1C_Base__Tier__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountSectionVisibilitiesSection1.Id, Name = 'CompareValue', T1C_FR__Value__c = '1'),

            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccount.Id, Name = 'PageTitle', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'ButtonContainer', T1C_FR__Value__c = 'buttonContainerLeft'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'ButtonId', T1C_FR__Value__c = 'mobileBack'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'DataField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'Label', T1C_FR__Value__c = 'Back'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'Order', T1C_FR__Value__c = '5'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'Type', T1C_FR__Value__c = 'URL'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsBack.Id, Name = 'URLPrefix', T1C_FR__Value__c = '#action=back&'),            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'ButtonContainer', T1C_FR__Value__c = 'buttonContainerRight'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'ButtonId', T1C_FR__Value__c = 'mobileForward'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'DataField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'Label', T1C_FR__Value__c = 'Forward'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'Type', T1C_FR__Value__c = 'URL'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountButtonsForward.Id, Name = 'URLPrefix', T1C_FR__Value__c = '#action=forward&'),            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountLookupsAccount.Id, Name = 'SearchFields', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountLookupsAccount.Id, Name = 'SObjectName', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountView.Id, Name = 'PageTitle', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountView.Id, Name = 'UnSavedChangesWarning', T1C_FR__Value__c = 'There are unsaved changes, do you want to discard changes and leave this page?'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'DataField', T1C_FR__Value__c = 'AnnualRevenue'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'Format', T1C_FR__Value__c = 'numbers'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'Label', T1C_FR__Value__c = 'Revenue in $Millions'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'MaxCharacters', T1C_FR__Value__c = '18'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewAnnualRevenue.Id, Name = 'PlaceholderText', T1C_FR__Value__c = 'Enter Revenue'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewDateTime.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Telephony_Last_Modified_Date__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewDateTime.Id, Name = 'Handler', T1C_FR__Value__c = 'DateTime'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewDateTime.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewTier.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Tier__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewTier.Id, Name = 'Handler', T1C_FR__Value__c = 'Picklist'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewTier.Id, Name = 'Order', T1C_FR__Value__c = '8'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewTier.Id, Name = 'Section', T1C_FR__Value__c = '8'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewName.Id, Name = 'DataField', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewName.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewName.Id, Name = 'Required', T1C_FR__Value__c = 'TRUE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewParentId.Id, Name = 'DataField', T1C_FR__Value__c = 'Parent.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewParentId.Id, Name = 'Handler', T1C_FR__Value__c = 'Lookup'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewParentId.Id, Name = 'Label', T1C_FR__Value__c = 'Parent Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewParentId.Id, Name = 'Order', T1C_FR__Value__c = '7'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewParentId.Id, Name = 'ParentField', T1C_FR__Value__c = 'ParentId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedBy.Id, Name = 'DataField', T1C_FR__Value__c = 'CreatedBy.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedBy.Id, Name = 'Label', T1C_FR__Value__c = 'Created By'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedBy.Id, Name = 'Order', T1C_FR__Value__c = '10'),            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedById.Id, Name = 'DataField', T1C_FR__Value__c = 'CreatedById'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedById.Id, Name = 'Label', T1C_FR__Value__c = 'Created By Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewCreatedById.Id, Name = 'Order', T1C_FR__Value__c = '10'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewFake.Id, Name = 'DataField', T1C_FR__Value__c = 'FieldThatDoesNotExist'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewFake.Id, Name = 'Order', T1C_FR__Value__c = '19'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewInactive.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Inactive__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewInactive.Id, Name = 'Handler', T1C_FR__Value__c = 'Checkbox'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsDynamicPageAccountViewInactive.Id, Name = 'Order', T1C_FR__Value__c = '9')
        };

        acct = dataFactory.makeAccount('Paper Street Soap Company');

        T1C_Base__Employee__c emp = dataFactory.makeLoggedInUserEmployee();
        insert emp;
    }
}