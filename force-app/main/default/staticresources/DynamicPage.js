(function($)	
{
    if(this.hasInit == null)
	{
		$('head').append('<meta content="initial-scale=1.0, user-scalable=0" name="viewport">');
		$('head').append('<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">');
		$("head").append('<script type="text/javascript" src="/soap/ajax/30.0/connection.js" />');
		$("head").append('<script type="text/javascript" src="/soap/ajax/30.0/apex.js" />');
		
		var loadingIndicator = $('<div class="loadingContainerWrapper"></div>').appendTo('.loadingIndicator');
		$('#loadingAnimation').appendTo(loadingIndicator);
		$('#loadingIndicatorText').appendTo(loadingIndicator);
	}
	
    var timezoneOffset;
	var userLocale = ACE.Salesforce.userLocale.replace('_','-');
    sforce.connection.sessionId = ACE.Salesforce.sessionId;
    var url = window.location.href;
	var recId = ACEUtil.getURLParameter('Id');
	if(recId == null)
	{
		recId = ACEUtil.getURLParameter('id');
	}

	var userId = ACEUtil.getURLParameter('userId');
	if(userId == null)
	{
		userId = ACE.Salesforce.userId;
	}

    var objectName = ACEUtil.getURLParameter('object');
	var mode = ACEUtil.getURLParameter('mode');
	var unsavedChanges = false;
	var unsavedWarningMessage;
	var describeSObjectResults = sforce.connection.describeSObjects([objectName]);
	var onSaveNavigation;
	var onSaveCloseWindow;
	var saveOnDupes;
	
    var componentHandlers = {
		'Input' : createInput,
		'Lookup' : createLookup,
		'Link' : createLink,
		'Picklist' : createPicklist,
		'RecordType' : createRecordTypePicklist, 
		'MultiSelect': createMultiSelectField,
		'Date' : createDatePicker,
		'DateTime' : createDateTimePicker,
		'Checkbox' : createCheckBoxItem,
		'UserInfo' : createUserInfoItem,
		'BigCheckbox' : createBigCheckBoxItem,
		'TextArea' : createNotesItem,
		'RichTextArea' : createRichTextItem,
		'File' : createFileItem
    }
    
    function DynamicPage()
    {
        this.init();
    }
	 
    DynamicPage.prototype.init = function()
    {
        var _this = this;

		this.recordMap = {};
		this.sectionVisibilityMap = {};
		this.fieldVisibilityMap = {};		
		this.fileMap = {};		

        if(this.cubeBody == null)
		{	
			this.isNew = false;
			this.rowMap = {};	
			this.subDivMap = {};
			this.userInputMap = {};
            this.containerMap = {};
			this.componentMap = {};
			this.headerRowMap = {};
			this.actionsComplete = false;
			this.toastHidden = false;
			this.requiredRecords = {};
			this.hasRecords = {};
			this.recordsErrorMessage = {};
			this.inputs = [];
			this.itemsToDelete = [];			
			this.hasRelatedRecords = false;
			this.hasFiles = false;
			this.inputMap = {};				
			this.picklistValueTracker = new Picklist();
			this.sectionVisibilityValueTracker = new SectionVisibility();
			this.sectionConfig = {};
			this.buttonMap = {};
			this.cubeBody = $('<div class="canvasFormDialog customCubeContainer"/>').appendTo('body');
			this.toastMessage = 'Saved';
			this.dynamicToast = $('<div id="dynamicToast">' + this.toastMessage + '</div>').appendTo(this.cubeBody);
			this.cubeFooter = $('<header class="cubeFaceFooter"></header>').appendTo(this.cubeBody);
			this.buttonContainer = $('<div class="headerWrapper"></header>').appendTo(this.cubeFooter);
			this.buttonContainerLeft = $('<div class="buttonContainer"></header>').appendTo(this.buttonContainer);
			//this.cancelButton = $('<button class="cancelButton">Cancel</button>').appendTo(this.buttonContainerLeft);      
			this.pageTileBox = $('<h1 class="ui-dialog-title"></h1>').appendTo(this.buttonContainer);
			this.buttonContainerRight = $('<div class="buttonContainer"></header>').appendTo(this.buttonContainer); 
			this.confirmButton = $('<button class="confirmButton">Save</button>').appendTo(this.buttonContainerRight);      
			this.cubeContent = $('<form id="cubeContent" class="powerCubeContent" autocomplete="off"></form>').appendTo(this.cubeBody);

			// This and the next listener are just for iOS not closing the picklists when tapping, dragging away
			$(this.cubeBody).on('touchstart touchend', function()
			{
				$("body").click();
			});

			$(this.cubeContent).on('touchstart touchend', function()
			{
				$("body").click();
			});
            this.requiredFieldsMap = {};
            this.hasInit = true;

            $(this.confirmButton).click(function()
			{
				_this.save();
            });
            /*
            $(this.cancelButton).click(function()
			{
				_this.cancelFunction();
			});			
			*/
		}
		
		// Redirect after creating the record
		if(this.isNew && recId != null)
		{
			window.location.assign(window.location.href + '&Id=' + recId);
		}

        $(this.cubeFooter).show();

		sforce.apex.execute("DynamicRecordPageController", "getConfig", {recordId : recId, refURL : url, usrId : userId}, 
		{ 
			onSuccess: function(resultString)
			{	
				var result = JSON.parse(resultString);

				_this.toastMessage = result.toastMessage;
				_this.toastDuration = result.toastDuration;
                timezoneOffset = result.timeZoneOffset;
                var machineOffset = new Date().getTimezoneOffset() * -60000;
                timezoneOffset = timezoneOffset - machineOffset;
				$(_this.pageTileBox).text(result.pageTitle);
				$("head").append('<title>' + result.pageTitle + '</title>');
				_this.subject = result.subject;

				if(_this.subject != null)
				{
					_this.recordMap[_this.subject.Id] = _this.subject;
				}
				else
				{
					_this.recordMap['isNew'] = {};
					_this.subject = {'isNew': true};
					_this.isNew = true;
				}	
				
				_this.subject = _this.processDefaults(result, _this.subject);
				
				saveOnDupes = result.saveOnDupes;
				onSaveCloseWindow = result.onSaveCloseWindow;
				onSaveNavigation = result.onSaveNavigation;
				
				_this.setupForm(result);
				
				unsavedWarningMessage = result.unsavedChangedWarningString;
				
				$.each(result.buttons,function(key, value)
				{
					createButton(_this,value);
				});
			}, 
			onFailure: function(error)
			{		
				console.log(error);
			} 
		});
	}
	
	DynamicPage.prototype.showToast= function(message,duration,_this,recId)
	{
		var thisToast = $('#dynamicToast');

		$(thisToast).text(message);
		$(thisToast).addClass('show');

		if(recId != null)
		{
			processAfterSave(_this, recId);
		}

		setTimeout(function()
		{
			$(thisToast).removeClass('show'); 
			$(_this.confirmButton).attr('disabled', false);
			_this.toastHidden = true;
			onSave(_this);
		}, duration);
	}

    DynamicPage.prototype.setupForm = function(config)
    {
        if(config != null)
        {
            this.mainSection = this.setupSectionHeader(config,objectName);
			this.setupSection(config, objectName);  
			setTimeout(function()
			{
				$('#cubeContent').scrollTop(1);
			},333);       
        }        
	}
	
	DynamicPage.prototype.processDefaults = function(config,record)
	{
		if(config.defCons != null && config.defCons.length > 0)
		{
			if(config.ignoreValueConfig != null && config.ignoreValueConfig.length > 0)
			{
				record['ignoreValues'] = config.ignoreValueConfig;
			}

			for(var x=0;x<config.defCons.length;x++)
			{
				var defCon = config.defCons[x];
				record[defCon.newRecordDefaultFieldName] = defCon.newRecordDefaultFieldValue;

				if(defCon.newRecordDefaultFieldParentName != null && defCon.newRecordDefaultFieldNameValue != null)
				{
					record[defCon.newRecordDefaultFieldParentName] = defCon.newRecordDefaultFieldNameValue;
				}
			}
		}

		return record;
	}

    DynamicPage.prototype.save = function()
    {
        var _this = this;
		$(this.confirmButton).attr('disabled', true);
        var requiredFields = {};
        
        if(this.subject == null)
        {
            this.subject = {};
        }
        
        $.each(this.inputMap, function(key, value)
		{            
			var disabled = ($(value).attr('disabled') == true);
			var required = ($(value).attr('isRequired') == 'true' || $(value).attr('isRequired') == true);
			
			var fieldName = key.split(':')[0];
			var thisObjectName = key.split(':')[1];
			var recordId = key.split(':')[2];

			var skip = false;
			var fieldValue = null;

			//_this.hasRecords[thisObjectName] = true;

			if(_this.recordMap[recordId] == null)
			{
				skip = true;
			}
			else if($(value).attr('isRTF') == 'true')
			{
				fieldValue = CKEDITOR.instances[key].getData();								
			}
			else if(value.isLookup == true)
			{
				fieldName = value.lookupField;
				fieldValue = $(value).attr('IdValue');
				if(fieldValue == "null" || !fieldValue)//Empty lookups assign the string value of "null" instead of the native null for some reason
				{
					fieldValue = '';
				}
			}
			else if(disabled || fieldName.split('.').length > 1 || value.grid != null)
			{
				skip = true;
			}
			else if(value.isMultiSelect == true)
			{
				var fieldValue = '';
				for(var x = 0;x < value._options.selectedItems.length;x++)
				{
					fieldValue += value._options.selectedItems[x].data + ';';
				}
			}
			else if(value.isDate == true)
			{				
				if($(value).val() != null && $(value).val() != '')
				{
					fieldValue = (new Date($(value).val()).getTime());
				}
				else
				{
					fieldValue = null;
				}
				skip = true;
			}
			else if(value.isDateTime == true && value.isDatePortion == true)
			{				
				var timeComponent = _this.inputMap[key + ':Time'];
				var timeVal = '';
				if(timeComponent != null)
				{
					timeVal = timeComponent.val();
				} 

				if(timeVal == '')
				{
					timeVal = '12:00am';
				}
				else if(timeVal.indexOf('am') == -1 && timeVal.indexOf('pm')== -1)
				{
					timeVal = moment($(value).val() + ' ' + timeVal).format('HH:mm');
				}
				else if(timeVal.indexOf('am') != -1)
				{
					timeVal = timeVal.replace('am', ' am');
				}
				else if(timeVal.indexOf('pm') != -1)
				{
					timeVal = timeVal.replace('pm', ' pm');
				}

				if($(value).val() != null && $(value).val() != '' && timeVal != null && timeVal != '') 
				{
					var fullValue = $(value).val() + ' ' + timeVal;
					fieldValue = moment(fullValue).toDate().getTime();
				}
				else
				{
					fieldValue = null;
				}

				//Skip 
				skip = true;
			}
			else if(value.isDateTime == true && value.isTimePortion == true)
			{
				skip = true;
			}
			else if(value.isCheckbox)
			{
				fieldValue = $(value).is(":checked");
			}
			else if(value.isPicklist)
			{
				var fieldValue = value.val();
			}
			else if(value.isNumber)
			{
				var fieldValue = value.val();
				fieldValue = fieldValue.replace(/[^0-9.]/g,'');
			}
			else
			{
				var fieldValue = $(value).val();
			}

			if(!skip)//Date fields should have null as the value if they are empty. Other fields need to be blank strings 
			{
				if(!fieldValue)
				{
					fieldValue = '';
				}
			}
			
			if(thisObjectName == objectName)
			{
				_this.subject[fieldName] = fieldValue;
			}
			else if(recordId != null && _this.recordMap[recordId] != null)
			{
				_this.recordMap[recordId][fieldName] = fieldValue;					
			}

			if(required && (fieldValue == null || fieldValue.length < 1))
			{
				requiredFields[$(value).attr('reqLabel')] = $(value).attr('reqLabel');
			}
        });

        var hasRequiredError = false;
		var fieldIdsNotFilledIn = 'The following fields have no value: ';

		$.each(requiredFields, function(key, value)
		{
			fieldIdsNotFilledIn += value + ', ';
			hasRequiredError = true;
		});

		fieldIdsNotFilledIn = fieldIdsNotFilledIn.substr(0,fieldIdsNotFilledIn.length-2);
		
		if(hasRequiredError)
		{
			ACEConfirm.show
			(
				'<p style="padding-left: 25px;">Required Fields Not Filled In</p><p style="padding-left: 25px;">' + fieldIdsNotFilledIn + '</p>',
				"Warning",
				[
					"OK::confirmButton"
				],
				function(result)
				{

				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog emptyFieldWarningDialog"}
			);
			$(_this.confirmButton).attr('disabled', false);
			return false;
		}

		Object.keys(_this.recordMap).forEach(function(key) 
		{
			if(_this.subject.Id != key)
			{	
				var itemToSave = _this.recordMap[key];
				
				if(!$.isEmptyObject(itemToSave))
				{
					_this.hasRecords[itemToSave.sobjectType] = true;
				}				
			}
		});
		
		var hasRequiredRecordsError = false;
		var hasRequiredRecordsErrorMessage = '';
		_this.hasRelatedRecords = false;

		$.each(_this.requiredRecords, function(key, value)
		{
			var hasRecs = _this.hasRecords[key];

			if(hasRecs == true)
			{
				_this.hasRelatedRecords = true;
			}

			if(value == true && hasRecs == false)
			{
				hasRequiredRecordsError = true;
				hasRequiredRecordsErrorMessage += _this.recordsErrorMessage[key] + '<br>';
			}
		});		

		if(hasRequiredRecordsError)
		{
			ACEConfirm.show
			(
				'<p style="padding-left: 25px;">' + hasRequiredRecordsErrorMessage + '</p>',
				"Warning",
				[
					"OK::confirmButton"
				],
				function(result)
				{

				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog emptyFieldWarningDialog"}
			);
			$(_this.confirmButton).attr('disabled', false);
			return false;
		}

        if(this.subject.Id == null)
        {
            this.subject['sobjectType'] = objectName;
		}  
		
		if(this.subject.Owner != null)
		{
			var owner = this.subject.Owner;
			delete this.subject.Owner;
			this.subject.OwnerId = owner.Id;
		}
		
		var jsonString = JSON.stringify(this.subject);

		sforce.apex.execute("DynamicRecordPageController", "saveInfo", {jsonString : jsonString, objectName : objectName, saveOnDupes : saveOnDupes},
		{ 
			onSuccess: function(resultString)
			{		
				var result = JSON.parse(resultString);

				if(result.returnId != null)
				{
					recId = result.returnId;
				}		
                
				unsavedChanges = false;

				var toastMessage = _this.toastMessage;

				if(result.message != null)
				{
 					toastMessage = result.message;
				}	
				
				_this.showToast(toastMessage,_this.toastDuration, _this, recId);
			}, 
			onFailure: function(error)
			{		
				console.log(error);
			} 
		});
	}

	function processAfterSave(_this, recId)
	{
		if(!_this.hasRelatedRecords && !_this.hasFiles)
		{
			_this.actionsComplete = true;
			onSave(_this);
		}
		else 				
		{
			saveRelatedRecords(_this, recId,_this.hasFiles);
			saveRelatedFiles(_this,recId);
		} 				

		if(_this.itemsToDelete.length > 0)
		{
			deleteRelatedInfos(_this.itemsToDelete, _this).promise().done();
		}
	}

	function saveRelatedFiles(_this, recId)
	{
		Object.keys(_this.fileMap).forEach(function(key) 
		{
			var fileInput = _this.fileMap[key];
			var filename = fileInput[0].files[0].name;
			var data = new FormData();
			var targetUrl = ACE.Salesforce.baseURL + "/services/data/v45.0/connect/files/users/" + userId;
			data.append("json", JSON.stringify({title: filename}));
			data.append("fileData", fileInput[0].files[0]);

			var xhr = new XMLHttpRequest();
			xhr.open('POST', targetUrl, true);
			xhr.setRequestHeader('Authorization', 'Bearer token' + ACE.Salesforce.sessionId);

			xhr.onloadend = function(response)
			{
				// handle http request failure
				if (response.target.status != 200 && response.target.status != 201)
				{
					console.log('content version creation rest api call failed');
					return; 
				}

				var contentDocumentId = JSON.parse(response.target.response).id;

				sforce.apex.execute("DynamicRecordPageController", "reparentFile", {contentDocumentId : contentDocumentId, parentId : recId}, 
				{ 
					onSuccess: function(result)
					{		
						console.log('update success!');
						_this.actionsComplete = true;
						onSave(_this);
					}, 
					onFailure: function(error)
					{		
						console.log('update failed!');
					} 
				});
			}
		
			xhr.send(data);
		});
	}

	function onSave(_this)
	{
		if(_this.toastHidden == true && _this.actionsComplete == true)
		{
			$(_this.confirmButton).attr('disabled', false);

			if(onSaveCloseWindow)
			{
				window.close();
			}
		}
	}
	
	function saveRelatedRecords(_this, recId, hasFiles)
	{
		var objectRecordsMap = {};

		Object.keys(_this.recordMap).forEach(function(key) 
		{
			if(_this.subject.Id != key)
			{	
				var itemToSave = _this.recordMap[key];
				if(itemToSave.isNew)
				{
					itemToSave[itemToSave.parentLookupField] = recId;
					delete itemToSave['isNew'];
					delete itemToSave['parentLookupField'];
					delete itemToSave['Id'];
				}

				var skipRecordSave = false;

				if(itemToSave.ignoreValues != null && itemToSave.ignoreValues.length > 0)
				{
					for(var x=0;x<itemToSave.ignoreValues.length;x++)
					{
						var igConf = itemToSave.ignoreValues[x];
						if(itemToSave[igConf.fieldName] == igConf.fieldValue)
						{
							skipRecordSave = true;
						}
					}

					delete itemToSave['ignoreValues'];
				}
				
				if(!$.isEmptyObject(itemToSave) && !skipRecordSave)
				{
					var items = objectRecordsMap[itemToSave.sobjectType];

					if(items == null)
					{
						items = [];
						objectRecordsMap[itemToSave.sobjectType] = items;
					}

					items.push(JSON.stringify(itemToSave));
				}	
			}
		});

		var i=0;
		Object.keys(objectRecordsMap).forEach(function(key) 
		{
			i++;
		});

		var x=0;
		Object.keys(objectRecordsMap).forEach(function(key) 
		{			
			var items = objectRecordsMap[key];

			saveRelatedInfos(items, key, saveOnDupes).promise().done(function()
			{
				x++

				if(x==i)
				{	
					if(hasFiles)
					{
						// Have the files save do the required actions.
					}			
					else 
					{
						_this.actionsComplete = true;
						onSave(_this);
					}
				}
			});
		});
	}

	function saveRelatedInfos(items, key, saveOnDupes)
	{
		var defer = new $.Deferred();

		var itemsList = [];
		for (let i = 0; i < items.length; i++)
		{
			var item = JSON.parse(items[i]);
			item["attributes"] = { "type": key };
			itemsList.push(item);
		}
		var itemsJSON = JSON.stringify(itemsList);

		sforce.apex.execute("DynamicRecordPageController", "saveRelatedInfo", { objsJSON : itemsJSON,  objectName : key,  saveOnDupes : saveOnDupes}, 
		{ 
			onSuccess: function(result)
			{		
				console.log('Saved related info');
				defer.resolve();
			}, 
			onFailure: function(error)
			{		
				console.log(result);
				console.log(event);
				defer.resolve();
			} 
		});

		return defer.promise();		
	}

	function deleteRelatedInfos(items, _this)
	{
		var defer = new $.Deferred();

		ACE.Remoting.call("DynamicRecordPageController.deleteRelatedInfo", [items], function(result,event)
		{
			if(event.status)
			{
				console.log('Deleted related info');
				_this.itemsToDelete = [];
			}
			else
			{
				console.log(result);
				console.log(event);
			}
			
			defer.resolve();
		});

		return defer.promise();		
	}

    DynamicPage.prototype.cancelFunction = function()
    {
		if(unsavedChanges)
		{
			ACEConfirm.show
			(
				'<p>' + unsavedWarningMessage + '</p>',
				"Warning",
				[
					"OK::confirmButton","Cancel::cancelButton"
				],
				function(result)
				{	
					if(result == 'OK')
					{
						this.init();   
					}
				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
			);
		}
		else
		{
			this.init();   
		}		     
    }

    DynamicPage.prototype.setupSectionHeader = function(config, objectName)
	{
		if(config == null)
		{
			return null;
		}

		var order = config[objectName + 'SectionOrder']

		if(this['MainFormSection'] == null)
		{
			this['MainFormSection'] = $('<div id="' + objectName + 'Details" class="section ' + objectName + 'Summary"></div>').appendTo(this.cubeContent);
		}

		return this[objectName + 'Section'];
    } 
    
    DynamicPage.prototype.setupSection = function(config, objectName)
	{
		if(config == null)
		{
			return null;
		}

		var section = this['MainFormSection'];
		var thisRecord = this.subject;
		var sections = {};
		var fieldConfig = config.fields;
		
		if(config.sectionConfigs != null)
		{
			for(var x=0;x<config.sectionConfigs.length;x++)
			{
				var sectionConf = config.sectionConfigs[x];
				this.sectionConfig[sectionConf.sectionNumber] = sectionConf.additionalClassNames;
			}
		}

		if(config.sectionVisibilities != null)
		{
			for(var x=0;x<config.sectionVisibilities.length;x++)
			{
				var sectionVis = config.sectionVisibilities[x];

				this.sectionVisibilityMap[sectionVis.sectionId] = sectionVis;

				var sectionVisibilities = this.fieldVisibilityMap[sectionVis.compareField];

				if(sectionVisibilities == null)
				{
					sectionVisibilities = [];
					this.fieldVisibilityMap[sectionVis.compareField] = sectionVisibilities;
				}

				sectionVisibilities.push(sectionVis);
			}
		}

		for(var x=0;x<fieldConfig.length;x++)
		{
			var thisFieldConfig = fieldConfig[x];
			thisFieldConfig.record = thisRecord;
			
            var thisSection = sections[thisFieldConfig.section];
            if(thisSection == null)
            {
				var additionalClassNames = this.sectionConfig[thisFieldConfig.section];
                thisSection = createDetailSectionContainer(thisFieldConfig.section,section,this,objectName,additionalClassNames);
                sections[thisFieldConfig.section] = thisSection;
            }

            var handler = componentHandlers[thisFieldConfig.handler];
            if(handler == null)
            {
                handler = createInput;
            }
			
            handler(this,thisSection,thisFieldConfig,false);        
		}

		if(config.relatedLists != null)
		{
			for(var i=0; i<config.relatedLists.length;i++)
			{
				var relListConfig = config.relatedLists[i];
				relListConfig.records = [];

				if(thisRecord[relListConfig.relationshipName] != null)
				{
					relListConfig.records = thisRecord[relListConfig.relationshipName];	
					delete thisRecord[relListConfig.relationshipName];
					this.hasRelatedRecords = true;
				} 
				else
				{
					if(relListConfig.defaultConfig != null)
					{
						var defCons = relListConfig.defaultConfig;
						var record = {Id : 'new' + relListConfig.sObjectName, isNew : true};
						
						if(relListConfig.ignoreValueConfig != null && relListConfig.ignoreValueConfig.length > 0)
						{
							record['ignoreValues'] = relListConfig.ignoreValueConfig;
						}

						if(defCons != null && defCons.length > 0)
						{
							for(var y=0;y<defCons.length;y++)
							{
								var defCon = defCons[y];
								record[defCon.newRecordDefaultFieldName] = defCon.newRecordDefaultFieldValue;

								if(defCon.newRecordDefaultFieldParentName != null && defCon.newRecordDefaultFieldNameValue != null)
								{
									record[defCon.newRecordDefaultFieldParentName] = defCon.newRecordDefaultFieldNameValue;
								}	
							}
						}
						
						this.hasRelatedRecords = true;
						relListConfig.records.push(record);
					}
				}

				var thisSection = sections[relListConfig.section];
				if(thisSection == null)
				{
					var thisSection = sections[relListConfig.section];
					var additionalClassNames = this.sectionConfig[relListConfig.section];
					thisSection = createDetailSectionContainer(relListConfig.section,section,this,objectName,additionalClassNames);
					sections[relListConfig.section] = thisSection;
				}
				
				createRelatedList(this,thisSection,relListConfig,relListConfig.records);
			}
		}
    }
    
    function createDetailSectionContainer(divId,section,_this,objectName,additionalClassNames)
	{
		if(divId == null)
		{
			return createDetailSectionContainer;
		}
		
		var thisDetailSection = _this.containerMap[objectName + divId]; 
		var sectionId = 'section' + divId;
		var sectionVis = _this.sectionVisibilityMap[sectionId];

		if(thisDetailSection == null)
		{
			if(additionalClassNames == null)
			{
				additionalClassNames = '';
			}

			thisDetailSection = $('<div order="' + divId + '" id="' + sectionId + '" class="subSection detailComponents ' + additionalClassNames + '">').appendTo(section);
			_this.containerMap[objectName + divId] = thisDetailSection;
		}
		
		var show = true;
			
		if(sectionVis != null)
		{
			show = getSectionVisibility(sectionVis, _this);

			if(sectionVis.compareField != null)
			{
				_this.sectionVisibilityValueTracker.watchValue(sectionVis.compareField,function()
				{
					var sectionVisibilities = _this.fieldVisibilityMap[sectionVis.compareField];

					if(sectionVisibilities != null && sectionVisibilities.length>0)
					{
						for(var x=0; x<sectionVisibilities.length;x++)
						{
							var thisSectionVis = sectionVisibilities[x];

							show = getSectionVisibility(thisSectionVis, _this);

							if(show)
							{
								$('#' + thisSectionVis.sectionId).show(333);
							}
							else
							{
								$('#' + thisSectionVis.sectionId).hide(333);
							}
						}	
					}
				});
			}
		}	

		if(show)
		{
			$(thisDetailSection).show(333);
		}
		else
		{
			$(thisDetailSection).hide(333);
		}
		
		return thisDetailSection;
	}
	
	function resortDivs(_this)
	{
		if(_this['MainFormSection'] == null)
		{
			return resortDivs;
		}
		
		var childDivs = $(_this['MainFormSection']).children();

		$(childDivs).sort(function(a,b){
			var an = a.getAttribute('order'),
				bn = b.getAttribute('order');
	
			if(an > bn) {
				return 1;
			}
			if(an < bn) {
				return -1;
			}
			return 0;
		}).detach().appendTo(_this['MainFormSection']);
	}

	function getCompareValue(compareField, value)
	{
		for(var x=0;x<compareField.split('.').length;x++)
		{
			var compFld = compareField.split('.')[x];
			value = value[compFld];
		}

		return value;
	}

	function getSectionVisibility(fieldConfig, _this)
	{
		var compareField = fieldConfig.compareField;
		var compareType = fieldConfig.compareType;
		var compareValue = fieldConfig.compareValue;
		var compareValueType = fieldConfig.compareValueType;
		var compareValueField = fieldConfig.compareValueField;

		if(compareField == null)
		{
			return true;
		}

		var fieldValue = getCompareValue(compareField, _this.subject);
		
		if(compareValueField != null)
		{
			if(_this.subject != null)
			{
				compareValue = getCompareValue(compareValueField, _this.subject);
			}
		}
		else // Cast the Compare value to the proper type
		{
			if(compareValueType == 'string')
			{
				compareValue = '' + compareValue;
			}
			else if(compareValueType == 'boolean')
			{
				compareValue = (compareValue == 'true');
			}
			else if(compareValueType == 'date')
			{
				if(compareValue == 'today')
				{
					var todayDate = new Date();	
					compareValue = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()).getTime();
				}
				else if(compareValue == 'now')
				{
					compareValue = new Date().getTime();
				}
				else
				{
					compareValue = new Date(compareValue).getTime();
				}	
			}
			else if(compareValueType == 'number')
			{
				compareValue = Number(compareValue);
			}
			else if(compareValueType == 'null')
			{
				compareValue = null;
			}
		}

		if(compareType == 'equals')
		{
			return (fieldValue == compareValue);
		}
		else if(compareType == 'notequals')
		{
			return (fieldValue != compareValue);
		}
		else if(compareType == 'greaterthan')
		{
			return (fieldValue > compareValue);
		}
		else if(compareType == 'lessthan')
		{
			return (fieldValue < compareValue);
		}
		else if(compareType == 'greaterthanequals')
		{
			return (fieldValue >= compareValue);
		}
		else if(compareType == 'lessthanequals')
		{
			return (fieldValue <= compareValue);
		}
		else if(compareType == 'contains')
		{
			return (fieldValue.indexOf(compareValue) != -1);
		}
		else 
		{
			return true;
		}
	}

    // Handler functions begins

	function createLink(_this,component,config,hasHeaders)
	{
		if(component == null || config == null || config.accessLevel == 'None')
		{
			return null;
		}
		
		var value = getValue(config);
		var label = config.label;
		var elementId = getElementId(config);
		var urlPrefix = config.urlPrefix;
		var urlSuffix = config.urlSuffix;
		var urlTarget = config.urlTarget;

		if(urlTarget == null)
		{
			urlTarget = '';
		}

		if(urlSuffix == null)
		{
			urlSuffix = '';
		}

		var thisInput = _this.componentMap[elementId + 'Link'];

		if(thisInput == null)
		{
			var subSectionItem = $('<div style="padding-top: 8px;" id="' + elementId + 'subSectionItem" class="componentContainer subSectionItem"></div>').appendTo(component);
			var editContainer = $('<div  id="' + elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(subSectionItem);
		
			thisInput = $('<a id="' + elementId + 'Link" class="detailsLink" target="' + urlTarget + '" href="' + urlPrefix + value + urlSuffix + '">View</a>').appendTo(editContainer);
			_this.componentMap[elementId + 'Link'] = thisInput;
			_this.inputs.push(thisInput);
		}
		else
		{
			$(thisInput).prop('href', urlPrefix + value + urlSuffix);
			$(thisInput).text('View');
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(thisInput).show();
			}
			else
			{
				$(thisInput).hide();
			}
		}
	
		return thisInput;
	}

    function createInput(_this,component,config,hasHeaders)
	{
		if(config == null || !config.enabled || config.fieldName == null || config.accessLevel == 'None')
		{
			return createInput;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			if(!isMobile)
			{
				if(config.format == null)
				{
					thisInput = $('<LA-Input Id="' + elementId + '" placeholder="' + config.placeHolderText + '" label="' + config.placeHolderText + '" value="' + value + '" maxchars="' +  config.maxLength + '" type="' + config.format + '"> </LA-Input>').appendTo(componentContainer.editContainer);	
				}
				else if(config.format == 'numbers')
				{
					var valueAsString = '';
					if(value != null && value != '')
					{
						valueAsString = new Number(value).toLocaleString();
					} 

					thisInput = $('<LA-Input Id="' + elementId + '" placeholder="' + config.placeHolderText + '" label="' + config.placeHolderText + '" value="' + valueAsString + '" maxchars="' +  config.maxLength + '" type="' + config.format + '"> </LA-Input>').appendTo(componentContainer.editContainer);	
				}
			}
			else
			{
				if(config.format == null)
				{
					thisInput = $('<LA-Input Id="' + elementId + '" placeholder="Tap to ' + config.placeHolderText + '" label="Tap to ' + config.placeHolderText + '" value="' + value + '" maxchars="' +  config.maxLength + '"> </LA-Input>').appendTo(componentContainer.editContainer);	
				}
				else if(config.format == 'numbers')
				{
					var valueAsString = '';
					if(value != null && value != '')
					{
						valueAsString = new Number(value).toLocaleString();
					} 

					thisInput = $('<input class="address-editor" type="number" placeholder="Tap to ' + config.placeHolderText + '" id="' + elementId + '" value="' + value + '"/>').appendTo(componentContainer.editContainer).hide();
					var displayDiv = $('<LA-Input Id="' + elementId + 'Display" placeholder="Tap to ' + config.placeHolderText + '" label="Tap to ' + config.placeHolderText + '" value="' + valueAsString + '"> </LA-Input>').appendTo(componentContainer.editContainer);	
					
					$(displayDiv).click(function ()
					{
						$(this).blur();
						$(thisInput).show();
						$(thisInput).focus();						
						$(this).hide();
					});

					$(thisInput).blur(function()
					{
						var val = $(this).val();
						var valAsString = null;
						if(val != null && val != '')
						{
							valAsString = new Number(val).toLocaleString();
						}
						
						$(displayDiv).val(valAsString);
						$(displayDiv).show();
						$(this).hide();
					});
				}
				else if(config.format == 'phone')
				{
					thisInput = $('<input class="address-editor" type="tel" placeholder="Tap to ' + config.placeHolderText + '" id="' + elementId + '"  value="' + value + '"/>').appendTo(componentContainer.editContainer);
				}
				else if(config.format == 'email')
				{
					thisInput = $('<input class="address-editor" type="email" placeholder="Tap to ' + config.placeHolderText + '" id="' + elementId + '"  value="' + value + '"/>').appendTo(componentContainer.editContainer);
				}
			}

			if(config.format == 'numbers')
			{
				thisInput.isNumber = true;
				$(thisInput).prop('isNumber', true);
			}

			$(thisInput).attr('isLookup', config.isLookup);

			$(thisInput).on('change', function() 
			{			
				unsavedChanges = true;
				$(this).prop('title',$(this).val());	
				if(!isMobile && $(this).prop('isNumber') == true)
				{
					var valAsString = '';
					var val = $(this).val();
					
					if(val != null && val != '')
					{
						val = val.replace(/[^0-9.]/g,'');
						valAsString = new Number(val).toLocaleString();
					}
					$(this).val(valAsString);
				}		
			});

			_this.inputMap[elementId] = thisInput;
		}
		else
		{			
			if(config.format == 'numbers')
			{
				var valAsString = '';
				var val = $(thisInput).val();
				if(val != null && val != '')
				{
					val = val.replace(/[^0-9.]/g,'');
					valAsString = new Number(val).toLocaleString();
				}
				
				$(thisInput).val(valAsString);
			}
			else
			{
				$(thisInput).val(value);
			}	
			$(thisInput).find('.la-input-clear-btn').hide();
		}

		$(thisInput).attr('disabled', disabled);
		$(thisInput).attr('isRequired', config.required);
		$(thisInput).attr('reqLabel',config.label);
		$(thisInput).prop('title',value);

		if(disabled)
		{
			$(thisInput).find('.la-input-clear-btn').hide();
		}
		//else
		//{
		//	$(thisInput).find('.la-input-clear-btn').show();
		//}

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}
		
		return thisInput;
	}

	function createRelatedList(_this,component,config,records)
	{
		if(config == null || config.label == null)
		{
			return createRelatedList;
		}

		if(config.hideButtons == true && (records == null || records.length == 0))
		{
			return null;
		}
		
		var elementId = config.relationshipName;

		var componentContainer = _this.componentMap[elementId];
		var headerRow = _this.headerRowMap[config.relationshipName];

		if(componentContainer == null)
		{
			config.fieldName = 'RelatedList';
			config.objectName = config.relationshipName;

			componentContainer = new SummaryCubeComponentContainer(_this,component,config,false);
			_this.componentMap[elementId] = componentContainer;

			$(componentContainer.editContainer).addClass('relatedListContainer');
			$(componentContainer.labelSpan).addClass('relatedListLabel');
			$(componentContainer.editContainer).css('flex-direction', 'column');

			var itemNumber = 0;
			var totalItems = 0;
			var recordSize = 0;

			if(records != null)
			{
				recordSize = records.length;
			}

			headerRow = createRelatedListHeader(_this,componentContainer.editContainer,config,recordSize);
			_this.headerRowMap[config.relationshipName] = headerRow;

			_this.componentMap[elementId] = componentContainer;

			// Put in Add new Item Button
			
			if(config.hideButtons == false && config.isFiles == false)
			{
				var addButton =  $('<div class="genericButton"><svg style="width:40px;height:40px;" viewBox="0 0 24 24">'+
				'<path fill="#CCC" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />'+
				'</svg></div>').appendTo(componentContainer.labelComponent);
			
				$(addButton).prop('headerRowId',$(headerRow).prop('id'));

				$(addButton).click(function(e)
				{
					e.preventDefault();
					e.stopPropagation();

					var item = {Id : config.sObjectName + itemNumber, sobjectType : config.sObjectName, isNew : true};
					itemNumber++;
					totalItems++;

					if(config.ignoreValueConfig != null && config.ignoreValueConfig.length > 0)
					{
						item['ignoreValues'] = config.ignoreValueConfig;
					}

					item[config.parentLookup] = _this.subject.Id;
					item.parentLookupField = config.parentLookup;
					config.record = item;
					var headerRowId = e.currentTarget.headerRowId;
					var thisHeaderRow = $('#'+headerRowId);
					var totItems = $(headerRow).prop('totalItems');
					createRelatedItem(_this,componentContainer.editContainer,config,totalItems,thisHeaderRow,true);

					totItems++;

					$(headerRow).show();
					$(headerRow).prop('totalItems',totItems);
					_this.hasRelatedRecords = true;
				});
			}
			else if(config.isFiles == true)
			{
				var addButton =  $('<div class="genericButton"><svg style="width:40px;height:40px;" viewBox="0 0 24 24">'+
				'<path fill="#CCC" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />'+
				'</svg></div>').appendTo(componentContainer.labelComponent);

				$(addButton).prop('headerRowId',$(headerRow).prop('id'));

				$(addButton).click(function(e)
				{
					e.preventDefault();
					e.stopPropagation();

					var item = {Id : config.sObjectName + itemNumber, sobjectType : config.sObjectName, isNew : true};
					itemNumber++;
					totalItems++;

					item[config.parentLookup] = _this.subject.Id;
					item.parentLookupField = config.parentLookup;
					config.record = item;
					var headerRowId = e.currentTarget.headerRowId;
					var thisHeaderRow = $('#'+headerRowId);
					var totItems = $(headerRow).prop('totalItems');
					createRelatedFileInputItem(_this,componentContainer.editContainer,config,totalItems,thisHeaderRow,true);

					totItems++;

					$(headerRow).show();
					$(headerRow).prop('totalItems',totItems);
				});
			}
			else
			{
				$('<div style="width: 100px;"></div>').appendTo(headerRow);
			}

			if(records != null)
			{
				for(var x=0;x<records.length;x++)
				{
					totalItems++;
					config.record = records[x];	
					config.record['sobjectType'] = config.sObjectName;

					if(config.record.isNew == true)
					{
						config.record.parentLookupField = config.parentLookup;				
					}

					createRelatedItem(_this,componentContainer.editContainer,config,totalItems,headerRow,(config.record.isNew == true));					
				}
			}

			Object.keys(_this.inputMap).forEach(function(key) 
			{
				var recordId = key.split(':')[2];

				if(_this.recordMap[recordId] == null)
				{
					delete _this.inputMap[key];
				}
			});
		}
		else
		{
			if(records != null)
			{
				var totalItems = 0;

				for(var x=0;x<records.length;x++)
				{
					totalItems++;
					config.record = records[x];	
					config.record['sobjectType'] = config.sObjectName;				
					createRelatedItem(_this,componentContainer.editContainer,config,totalItems,headerRow,false);					
				}
			}

			var newRecs = $(componentContainer.editContainer).find('.isNew').remove();
		}
	}

	function createRelatedListHeader(_this,component,config,totalItems)
	{
		var hasHeaders = false; //(totalItems > 0);
		
		var row = $('<div  id="' + config.relationshipName + '" class="cubeComponentContainer"></div>');
		$(row).prop('totalItems',totalItems);

		_this.requiredRecords[config.sObjectName] = config.requireRecords;
		_this.hasRecords[config.sObjectName] = false;
		_this.recordsErrorMessage[config.sObjectName] = config.requireRecordsErrorMessage;

		if(config.showHeader == true)
		{
			$(row).appendTo(component);
		}

		if(config.fields != null)
		{
			for(var x=0;x<config.fields.length;x++)
			{
				var headerConfig = config.fields[x];

				if(headerConfig.accessLevel == 'None')
				{
					continue;
				}

				var subSectionItem = $('<div class="componentContainer subSectionItem"></div>').appendTo(row);
				var labelComponent =  $('<div title="' + config.fields[x].helpText  + '" class="label">' + config.fields[x].label + '</div>').appendTo(subSectionItem);
			}
			$('<div style="width: 60px;"></div>').appendTo(row);
		}

		if(hasHeaders)
		{
			$(row).show();
		}
		else
		{
			$(row).hide();
		}

		return row;
	}

	function createRelatedFileInputItem(_this,component,config,totalItems,headerRow,isNew)
	{
		var rowId = getRowId(config);
		var hasHeaders = true;
		var isNewClass = '';
		var isNewRow = false;
		if(isNew)
		{
			isNewClass = 'isNew';
		}

		var row = _this.rowMap[rowId];
		if(row == null)
		{
			row = $('<div  id="' + rowId + 'editContainer" class="cubeComponentContainer relatedList ' + isNewClass + '"></div>').appendTo(component);
			_this.rowMap[rowId] = row;
			isNewRow = true;
		}

		createFileItem(_this,row,config,hasHeaders);  
	}

	function createRelatedItem(_this,component,config,totalItems,headerRow,isNew)
	{
		var rowId = getRowId(config);
		var hasHeaders = true;
		var isNewClass = '';
		var isNewRow = false;
		if(isNew)
		{
			isNewClass = 'isNew';
		}
 
		var row = _this.rowMap[rowId];
		var subDiv = _this.subDivMap[rowId];
		if(row == null)
		{
			row = $('<div  id="' + rowId + 'editContainer" class="cubeComponentContainer relatedList ' + isNewClass + '"></div>').appendTo(component);
			subDiv = $('<div class="relatedItemFieldContainer cubeComponentContainer" style="flex-wrap: wrap;"></div>').appendTo(row);

			_this.subDivMap[rowId] = subDiv;
			_this.rowMap[rowId] = row;
			isNewRow = true;
		}

		if(config.fields != null)
		{
			for(var x=0;x<config.fields.length;x++)
			{
				var thisFieldConfig = config.fields[x];
				thisFieldConfig.record = config.record;

				var handler = componentHandlers[thisFieldConfig.handler];
				if(handler == null)
				{
					handler = createInput;
				}

				// Create label for the item
				var headerAndItemContainer = $('<div class="componentContainer subSectionItem" style="align-items: unset;"></div>').appendTo(subDiv);
				var labelComponent =  $('<div title="' + thisFieldConfig.helpText  + '" class="label">' + thisFieldConfig.label + '</div>').appendTo(headerAndItemContainer);
				
				handler(_this,headerAndItemContainer,thisFieldConfig,hasHeaders);        
			}

			if(isNewRow)
			{
				if(config.hideButtons == false)
				{

					var removeButton = $('<div class="genericButton"><svg style="width:40px;height:40px;margin-right: -23px;" viewBox="0 0 24 24">'+
					'<path fill="#CCC" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M7,13H17V11H7" />'+
					'</svg></div>');
					
					if(config.record.isNew == true)
					{
						$(removeButton).appendTo(subDiv);
					}
					else
					{
						$('<div class="genericButton"></div>').appendTo(subDiv);
					}
					
					$(removeButton).prop('recordId',config.record.Id);
					$(removeButton).prop('isNew',config.record.isNew);
					$(removeButton).prop('headerRowId',$(headerRow).prop('id'));

					$(removeButton).click(function(e)
					{
						var thisButton = e.currentTarget;
						var recordId = $(thisButton).prop('recordId');
						var isNew = $(thisButton).prop('isNew');
						var headerRowId = $(thisButton).prop('headerRowId');
						var headerRow = $('#' + headerRowId);
						var totItems = $(headerRow).prop('totalItems');

						delete _this.recordMap[recordId];

						if(isNew != true)
						{
							_this.itemsToDelete.push({Id : recordId});					
						}

						$(row).remove();
						e.preventDefault();
						e.stopPropagation();

						totItems--;				
						$(headerRow).prop('totalItems',totItems);

						if(totItems == 0)
						{
							$(headerRow).hide();
						}
					});
				}
				else
				{
					$('<div style="width: 100px;"></div>').appendTo(row);
				}
			}

			_this.recordMap[config.record.Id] = config.record;
		}
	}
    
    function createLookup(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createInput;
		}

		var value = getValue(config);

        var idValue;
        if(config.record[config.parentField] != null)
        {
            idValue = config.record[config.parentField];
        }
        else
        {
			idValue = config.fieldNameData;
			if(value == null || value.length == 0 && config.fieldNameLabel != null)
			{
				value = config.fieldNameLabel;
			}
        }   

        value = unescape(value);

		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			var placeHolderText = config.placeHolderText;
			if(isMobile)
			{
				placeHolderText = 'Tap to ' + placeHolderText;
			}
			thisInput = $('<input Id="' + elementId + '" placeholder="' + placeHolderText + '" IdValue="' + idValue + '" value="' + value + '" label="' + placeHolderText + '"> </input>').appendTo(componentContainer.editContainer);
			thisInput.isLookup = true;
			thisInput.lookupField = config.parentField;
			thisInput.lookupObjectField = config.lookupObjectField;
			thisInput._this = _this;

			thisInput.on('change', function() 
			{			
				unsavedChanges = true;
			});

			_this.inputMap[elementId] = thisInput;

			var lookupConfig = {};
			lookupConfig['sobjectName'] = config.lookupObjectName;
			lookupConfig['additionalQueryFields'] = config.lookupSearchFields;
			var lookupNameField = config.lookupNameField;
			var lookupSearchFieldList = [];
			var columns = [];
			for(var x=0;x<config.lookupSearchFieldsConfig.length;x++)
			{
				var searchField = config.lookupSearchFieldsConfig[x];
				lookupSearchFieldList.push(searchField.fieldName);
				columns.push({'id':searchField.fieldName,'field':searchField.fieldName,'style':searchField.style,'apexType':"ACECoreController.Column"});
			}
			lookupConfig['columnList'] = columns;
			
			setTimeout(function()
			{
				autoComplete = new SearchAutoComplete($(thisInput,lookupNameField),
				{
					mode: "CUSTOM:" + config.lookupFeatureName,
					maintainSelectedItem: true,
					coverageOnly: false,
					additionalQueryFields: config.lookupSearchFields,
					additionalWhereClause: config.additionalWhereClause,
					config: lookupConfig,
					select: function(item)
					{
						var _this = thisInput._this;
						if(item != null)
						{
							$(thisInput).attr('IdValue',item.Id);
							if(lookupNameField != 'Name')
							{
								item.Name = item[lookupNameField];
							}

							if(thisInput.lookupObjectField != null)
							{

							}
						}
						else
						{
							$(thisInput).attr('IdValue','');
						}
						unsavedChanges = true;					
					}		
				});	
				
				autoComplete.setSelectedItem(idValue);
			}, 100);
		}
		else
		{
			$(thisInput).val(value);
			$(thisInput).find('.la-input-clear-btn').hide();
		}

		$(thisInput).attr('disabled', disabled);
		$(thisInput).attr('isRequired', config.required);
		$(thisInput).attr('reqLabel',config.label);

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}

		return thisInput;
	}
	
	function createRichTextItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createRichTextItem;
		}
		
		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);

			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{
			thisInput = $('<textarea class="powerCubeTextarea" Id="' + elementId + '" maxchars="' +  config.maxLength + '">' + value + '</textarea>').appendTo(componentContainer.editContainer);
			CKEDITOR.replace(elementId);
			$(thisInput).attr('isRTF',true);

			_this.inputMap[elementId] = thisInput;

			$(thisInput).attr('disabled',disabled);
			$(thisInput).attr('isRequired',config.required);		
			$(thisInput).attr('reqLabel',config.label);

			if(config.required)
			{
				$(thisInput).addClass('required');
			}
			else
			{
				$(thisInput).removeClass('required');
			}
		}

		CKEDITOR.instances[elementId].setData(value);
	
		return thisInput;
	}
    
    function createNotesItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createNotesItem;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);

			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			thisInput = $('<textarea class="powerCubeTextarea" Id="' + elementId + '" maxchars="' +  config.maxLength + '">' + value + '</textarea>').appendTo(componentContainer.editContainer);

			thisInput.on('change', function() 
			{			
				unsavedChanges = true;
				var thisValue = $(this).val();
				$(this).prop('title',thisValue);
				$(this).prop('textContent',thisValue);
				$(this).prop('customTitle',thisValue);
				$(this).prop('defaultValue',thisValue);
				$(this).prop('innerHTML',thisValue);
			});

			_this.inputMap[elementId] = thisInput;
		}
		else
		{
			$(thisInput).val(value);
			$(thisInput).prop('title',value);
			$(thisInput).prop('textContent',value);
			$(thisInput).prop('customTitle',value);
			$(thisInput).prop('defaultValue',value);
			$(thisInput).prop('innerHTML',value);
		}

		$(thisInput).attr('disabled',disabled);
		$(thisInput).attr('isRequired',config.required);		
		$(thisInput).attr('reqLabel',config.label);

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}

		return thisInput;
	}

	function createUserInfoItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None' || config.record == null || (config.record != null && config.record.isNew == true))
		{
			return createUserInfoItem;
		}

		var photoURL = config.record['CreatedBy']['FullPhotoUrl'];
		var userName = config.record['CreatedBy']['Name'];
		var userTitle = config.record['CreatedBy']['Title'];

		var elementId = getElementId(config);
		
		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
			//$(componentContainer.subSectionItem).addClass('userImageComponent');
		}

		var thisInput = _this.userInputMap[elementId];

		if(thisInput == null)
		{
			var subSectionItem = $('<div style="padding-top: 5px;"></div>').appendTo(componentContainer.editContainer);

			var imgDiv = $('<div style="max-width: 50px;">').appendTo(subSectionItem);

			if(photoURL != null)
			{
				thisInput = $('<img height="50" width="50" style="border-radius:50%;" src="' + photoURL + '"></img>').appendTo(imgDiv);
			}

			var nameAndTitle = $('<div style="display: grid;">').appendTo(subSectionItem);

			if(userName != null)
			{
				$('<div class="demoName">'+userName+'</div>').appendTo(nameAndTitle);
			}

			if(userTitle != null)
			{
				$('<div class="demoTitle">'+userTitle+'</div>').appendTo(nameAndTitle);
			}
			
			_this.userInputMap[elementId] = thisInput;
		}
	}
	
	function createFileItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createFileItem;
		}

		_this.hasFiles = true;

		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.fileMap[elementId];

		if(thisInput == null)
		{
			thisInput = $('<input type="file" id="' + elementId + '" title="' + config.helpText + '"></input>').appendTo(component);
			thisInput.isFile = true;

			_this.fileMap[elementId] = thisInput;
		}
	}

	function createBigCheckBoxItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createBigCheckBoxItem;
		}

		var acceptImage = '<svg style="width:100px;height:100px;padding-right:25px" viewBox="0 0 24 24"><path fill="#1faa00" d="M12 2C6.5 2 2 6.5 2 12S6.5 22 12 22 22 17.5 22 12 17.5 2 12 2M12 20C7.59 20 4 16.41 4 12S7.59 4 12 4 20 7.59 20 12 16.41 20 12 20M16.59 7.58L10 14.17L7.41 11.59L6 13L10 17L18 9L16.59 7.58Z" /></svg>';
		var rejectImage = '<svg style="width:100px;height:100px" viewBox="0 0 24 24"><path fill="#9b0000" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" /></svg>';

		config.required = false;
		
		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);
		
		var checked = '';
		if(value == true)
		{
			checked = 'checked';
		}

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			$(componentContainer.editContainer).css('align-self', 'center');
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			thisInput = $('<input type="checkbox" id="' + elementId + '" ' + checked + ' title="' + config.helpText + '"></input>' ).checkbox();			
			thisInput.isCheckbox = true;		
			_this.inputMap[elementId] = thisInput;
			
			var mainContainer = $('<div style="width:250px;height:100px;display: flex;"></div>').appendTo(componentContainer.editContainer);
			var acceptContainer = $('<div style="width:100px;height:100px;border-radius: 50%;margin-right:50px"></div>').appendTo(mainContainer);
			var rejecttContainer = $('<div style="width:100px;height:100px;border-radius: 50%;"></div>').appendTo(mainContainer);
			$(acceptImage).appendTo(acceptContainer);
			$(rejectImage).appendTo(rejecttContainer);
			
			if($(thisInput).is(":checked"))
			{
				$(acceptContainer).css('background-color','#9cff57');		
			}
			else
			{
				$(rejecttContainer).css('background-color','#ff5131');
			}

			$(acceptContainer).click(function() 
			{		
				$(acceptContainer).css('background-color','#9cff57');		
				$(rejecttContainer).css('background-color','#fff');
				
				$(thisInput).prop('checked', true);
				_this.picklistValueTracker[elementId] = $(thisInput).is(":checked"); 
				_this.subject[config.fieldName] = $(thisInput).is(":checked");
				_this.sectionVisibilityValueTracker[config.fieldName] = $(thisInput).is(":checked");
				unsavedChanges = true;
			});

			$(rejecttContainer).click(function() 
			{
				$(acceptContainer).css('background-color','#fff');
				$(rejecttContainer).css('background-color','#ff5131');

				$(thisInput).prop('checked', false); 
				_this.picklistValueTracker[elementId] = $(thisInput).is(":checked"); 
				_this.subject[config.fieldName] = $(thisInput).is(":checked");
				_this.sectionVisibilityValueTracker[config.fieldName] = $(thisInput).is(":checked");

				unsavedChanges = true;
			});
		}
		else
		{
			$(thisInput).prop("checked",(value == true));			
		}

		$(thisInput).attr("disabled", disabled);
	}
    
    function createCheckBoxItem(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createCheckBoxItem;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);
		var checked = '';
		if(value == true)
		{
			checked = 'checked';
		}

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			thisInput = $('<input type="checkbox" id="' + elementId + '" ' + checked + ' title="' + config.helpText + '"></input>' )
			.appendTo(componentContainer.editContainer)
			.checkbox();
			
			thisInput.isCheckbox = true;
			$(thisInput).attr("disabled", disabled);

			$('.control.checkbox').addClass("summaryCheckbox");
			$('.control.checkbox').css("position","relative");
			$('.control.checkbox').css("height","20px");

			$(thisInput).parent().children('.control-indicator').css('position','relative');
			
			_this.inputMap[elementId] = thisInput;
				
			$(thisInput).click(function() 
			{
				_this.picklistValueTracker[elementId] = $(thisInput).is(":checked"); 
				_this.subject[config.fieldName] = $(thisInput).is(":checked");
				_this.sectionVisibilityValueTracker[config.fieldName] = $(thisInput).is(":checked");
				
				unsavedChanges = true;
			});
		}
		else
		{
			$(thisInput).prop("checked",(value == true));			
		}

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}

		$(thisInput).attr("disabled", disabled);
    }
    
    function createRecordTypePicklist(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createRecordTypePicklist;
		}

		var picklistValues = [];
		var value = config.record[config.fieldName];
		var elementId = getElementId(config);

		if(value == null)
		{
			picklistValues.push({data : '', label : 'Please Select a Record Type', defaultSelected : true});
		}		

		if(_this._config.recordTypeInfo != null)
		{
			for(var x=0;x<_this._config.recordTypeInfo.length;x++)
			{
				var thisPicklistValue = _this._config.accountRecordTypeInfo[x];
				picklistValues.push({data : thisPicklistValue.rtId, label : thisPicklistValue.label, defaultSelected : (value == thisPicklistValue.rtId)});
			}	
		}		
		else
		{
			return null;
		}

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}
		var disabled = (config.accessLevel == 'Read');

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{		
			thisInput = new ACEMenuButton($(componentContainer.editContainer),{ dataProvider : picklistValues});
			_this.picklistValueTracker[elementId] = value;
			thisInput.isPicklist = true;

			thisInput.on('click', function() 
			{
				unsavedChanges = true;
			});
			
			_this.inputMap[elementId] = thisInput;
		}
		else
		{
			thisInput.set("dataProvider",picklistValues);
			thisInput.setSelectedItem(value);	
		}

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}

		$(thisInput).prop('disabled',disabled);
	}

    function createPicklist(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createPicklist;
		}
		
		var value = getValue(config);
		var elementId = getElementId(config);
		
		if(config.fieldNameData != null && (value == null || value.length == 0))
		{
			value = config.fieldNameData;
		}

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}
		var disabled = (config.accessLevel == 'Read');

		var thisInput = _this.inputMap[elementId];

		var picklistValues = [];

		if(config.controllingField != null)
   		{
			picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);

			_this.picklistValueTracker.watchPicklistValue(config.controllingField.name+ ':' + config.objectName + ':' + config.recId,function()
   			{
   				picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);   				
   			});
		}
		else
		{
			if(config.picklistValues != null && config.picklistValues.length > 0)
			{
				for(var x=0;x<config.picklistValues.length;x++)
				{	
					var thisPicklistValue = config.picklistValues[x];
					var isDefault = ((value == null && thisPicklistValue.isDefault) || (value != null && value == thisPicklistValue.value));
					picklistValues.push({data : thisPicklistValue.value, label : thisPicklistValue.label, defaultSelected : isDefault});
				}
			}
		}

		if(thisInput == null)
		{		
			thisInput = new ACEMenuButton($(componentContainer.editContainer),{ dataProvider : picklistValues});
			$(thisInput).addClass('dynamicPicklist');
			_this.picklistValueTracker[elementId] = value;
			thisInput.isPicklist = true;
			
			thisInput.on('click', function() 
			{
				var pickVal = this.val();
				_this.picklistValueTracker[elementId] = pickVal;
				
				unsavedChanges = true;
			});
			
			_this.inputMap[elementId] = thisInput;
		}
		else
		{
			thisInput.set("dataProvider",picklistValues);
			thisInput.setSelectedItem(value);	
			_this.picklistValueTracker[elementId] = value;		
		}

		$(thisInput).prop('disabled',disabled);
		$(thisInput).attr('isRequired',config.required);
		$(thisInput).attr('reqLabel',config.label);

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}

		if(config.controllingField != null)
		{
			_this.picklistValueTracker.watchPicklistValue(config.controllingField.name + ':' + config.objectName + ':' + config.recId,function()
			{
				picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);  	
				thisInput.set("dataProvider",picklistValues);
			});
		}
	}
	  
    function createDateTimePicker(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createDatePicker;
		}

		var value = getDateTimeValue(config);
		var elementId = getElementId(config);
		var timeElementId = elementId + ':Time';
		var componentContainer = _this.componentMap[elementId];
		var disabled = (config.accessLevel == 'Read');
		var joinDiv = _this.componentMap[elementId + 'JoinDiv'];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
			joinDiv = $('<div class="dateTimePicker"/>').appendTo(componentContainer.editContainer);
			_this.componentMap[elementId + 'JoinDiv'] = joinDiv;
		}

		var datePicker = _this.inputMap[elementId];
		var dateTimePicker = _this.inputMap[timeElementId];

		if(datePicker == null)
		{
			if(isMobile)
			{
				if(value != null)
				{
					datePicker = $('<input class="hasDatepicker" value="' + moment(value).format('YYYY-MM-DD') + '" placeholder="Select a Date" Id="' + elementId + '" type="date" style="width: auto;">').appendTo(joinDiv);
				}
				else
				{
					datePicker = $('<input class="hasDatepicker" placeholder="Select a Date" Id="' + elementId + '" type="date" style="width: auto;">').appendTo(joinDiv);
				}
			}
			else
			{
				datePicker = $('<input Id="' + elementId + '" value="' + value + '"/>')
					.datepicker($.datepicker.regional[userLocale])
					.appendTo(joinDiv);
				
				$(datePicker).datepicker("option", "closeText", "Clear");
			}

			datePicker.isDateTime = true;
			datePicker.isDatePortion = true;

			_this.inputMap[elementId] = datePicker;
		
			datePicker.on('change', function() 
			{
				unsavedChanges = true;
			});
		}

		if(dateTimePicker == null)
		{
			if(isMobile)
			{
				if(value != null)
				{
					dateTimePicker = $('<input class="ui-timepicker-input" value="' + moment(value).format('HH:mm') + '" placeholder="Select a Time" Id="' + timeElementId + '" type="time"/>').appendTo(joinDiv);
				}
				else
				{
					dateTimePicker = $('<input class="ui-timepicker-input" placeholder="Select a Time" Id="' + timeElementId + '" type="time"/>').appendTo(joinDiv);
				}
			}
			else
			{
				dateTimePicker = $('<input Id="' + timeElementId + '" value="' + value + '"/>')
				.timepicker()
				.appendTo(joinDiv);
			}

			dateTimePicker.isDateTime = true;
			dateTimePicker.isTimePortion = true;

			_this.inputMap[timeElementId] = dateTimePicker;
		
			dateTimePicker.on('change', function() 
			{
				unsavedChanges = true;			
			});
		}

		if(!isMobile)
		{
			if(value != null)
			{
				datePicker.datepicker("setDate", new Date(value));
				$(dateTimePicker).val(moment(value).format('h:mm a'));
			}		
			else
			{
				datePicker.datepicker("setDate", null);
				$(dateTimePicker).val('');
			}
		}

		$(datePicker).prop('disabled',disabled);
		$(dateTimePicker).prop('disabled',disabled);
		$(datePicker).attr('isRequired',config.required);
		$(dateTimePicker).attr('isRequired',config.required);	
		$(datePicker).attr('reqLabel',config.label);
		$(dateTimePicker).attr('reqLabel',config.label);
		
		if(config.required)
		{
			$(datePicker).addClass('required');
			$(dateTimePicker).addClass('required');
		}
		else
		{
			$(datePicker).removeClass('required');
			$(dateTimePicker).removeClass('required');
		}
	}

	function createDatePicker(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createDatePicker;
		}
		
		var value = getDateTimeValue(config);
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var disabled = (config.accessLevel == 'Read');

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var datePicker = _this.inputMap[elementId];

		if(datePicker == null)
		{
			if(isMobile)
			{
				if(value != null)
				{
					datePicker = $('<input class="hasDatepicker" value="' + moment(value).format('YYYY-MM-DD')  + '" placeholder="Select a Date" Id="' + elementId + '" type="date" style="width: auto;">').appendTo(componentContainer.editContainer);
				}
				else
				{
					datePicker = $('<input class="hasDatepicker" placeholder="Select a Date" Id="' + elementId + '" type="date" style="width: auto;">').appendTo(componentContainer.editContainer);
				}
			}
			else
			{
				$.datepicker._defaults.closeText			
				datePicker = $('<input Id="' + elementId + '" onfocus="blur();" />')
					.datepicker($.datepicker.regional[userLocale])
					.appendTo(componentContainer.editContainer);
				$(datePicker).datepicker("option", "closeText", "Clear");
			}

			datePicker.isDate = true;
			
			_this.inputMap[elementId] = datePicker;
		
			datePicker.on('change', function() 
			{
				unsavedChanges = true;
			});
		}

		if(!isMobile)
		{
			if(value != null)
			{
				datePicker.datepicker("setDate", new Date(value));
			}		
			else
			{
				datePicker.datepicker("setDate", null);
			}
		}
		
				
		$(datePicker).prop('disabled',disabled);
		$(datePicker).attr('isRequired',config.required);
		$(datePicker).attr('reqLabel',config.label);
		
		if(config.required)
		{
			$(datePicker).addClass('required');
		}
		else
		{
			$(datePicker).removeClass('required');
		}
    }
    
    function createMultiSelectField(_this,component,config,hasHeaders)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createMultiSelectField;
		}

		var value = getValue(config);
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config,hasHeaders);
			_this.componentMap[elementId] = componentContainer;
		}

		var disabled = (config.accessLevel == 'Read');
		var valueDivText = 'click here to add values';
		var thisInput = _this.inputMap[elementId];

		var selectedItems = [];
		
		if(value != null && value != '')
		{
			valueDivText = value;
			var selItems = value.split(';');
			for(var i = 0; i<selItems.length;i++)
			{
				selectedItems.push(selItems[i]);
			}
		}

		var valueDiv = _this.componentMap[elementId + 'ValueDiv'];
		if(valueDiv != null)
		{
			$(valueDiv).text(valueDivText);
			$(valueDiv).prop('title',valueDivText);
			$(valueDiv).prop('textContent',valueDivText);
			$(valueDiv).prop('customTitle',valueDivText);
			$(valueDiv).prop('defaultValue',valueDivText);
			$(valueDiv).prop('innerHTML',valueDivText);
		}
		else
		{	
			valueDiv = $('<div id="' + elementId + 'ValueDiv" style="min-width:150px" class="cubeComponentContainer multiSelectValue">' + valueDivText + '</div>').appendTo(componentContainer.editContainer);		
			_this.componentMap[elementId + 'ValueDiv'] = valueDiv;
		}	

		var picklistValues = [];
		var availPicklistValues = [];
		var selPicklistValues = [];

		if(config.controllingField != null)
   		{
			picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);

			_this.picklistValueTracker.watchPicklistValue(config.controllingField.name+ ':' + config.objectName + ':' + config.recId,function()
   			{
   				picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);   				
   			});
		}
		else
		{
			if(config.picklistValues != null && config.picklistValues.length > 0)
			{
				for(var x=0;x<config.picklistValues.length;x++)
				{	
					var thisPicklistValue = config.picklistValues[x];
					var isDefault = ((value == null && thisPicklistValue.isDefault) || (value != null && thisPicklistValue.value));
					picklistValues.push({data : thisPicklistValue.value, label : thisPicklistValue.label, defaultSelected : isDefault});
				}
			}
		}
	
		if(picklistValues != null && picklistValues.length > 0)
		{
			for(var x=0;x<picklistValues.length;x++)
            {
				var thisPicklistValue = picklistValues[x];
				thisPicklistValue.defaultSelected = false;
				var isSelected = false;
				
				for(var i=0;i<selectedItems.length;i++)
				{
					var fldVal = selectedItems[i];
					
					if(fldVal == thisPicklistValue.data)
					{
						isSelected =true;
						break;
					}
				}
				
				if(isSelected)
				{
					selPicklistValues.push(thisPicklistValue);
				}
				else
				{
					availPicklistValues.push(thisPicklistValue);
				}	
			}
		}

		var displayDiv = _this.componentMap[elementId + 'displayDiv'];

		if(displayDiv == null)
		{
			displayDiv = $('<div class="multiSelectContainer"/>').appendTo(component);
			_this.componentMap[elementId + 'displayDiv'] = displayDiv;

			$(displayDiv).hide();
					
        	$(valueDiv).click(function()
        	{
				if(!disabled)
				{					
			    	if($(displayDiv).is(':visible'))
					{	
				        $(displayDiv).hide(333);
				        $(displayDiv).removeClass('showHiddenContainer');
			    	}
			    	else
			    	{
			    		$(displayDiv).show(333);
				        $(displayDiv).addClass('showHiddenContainer');
			    	}
				}	
        	});
		}

		var selItemPlValues = [];
		for(var x=0;x<selectedItems.length;x++)
		{
			var thisVal = selectedItems[x];
			selItemPlValues.push({data: thisVal, label: thisVal, defaultSelected: false});
		}
			
		if(thisInput == null)
		{			
			thisInput = new ACEMultiSelect({dataProvider : picklistValues});
			thisInput.isMultiSelect = true;

			_this.inputMap[elementId] = thisInput;

	    	thisInput.selectedItems(selPicklistValues);
	    	thisInput.appendTo(displayDiv);
                	
    		$(thisInput.addButton).click(function(event) 
			{    			
				event.preventDefault();		
				setMultiSelectValues(thisInput,picklistValues,config,valueDiv);
			});
			
			$(thisInput.removeButton).click(function(event) 
			{    			
				event.preventDefault();		
				setMultiSelectValues(thisInput,selItemPlValues,config,valueDiv);
			});
			
			if(config.controllingField != null)
			{
				_this.picklistValueTracker.watchPicklistValue(config.controllingField.name + ':' + config.objectName + ':' + config.recId,function()
				{
					picklistValues = getPicklistValuesForDependantPicklist(_this,config,true);  	
					thisInput.dataProvider(picklistValues);
					thisInput.selectedItems(selPicklistValues);
					
					setMultiSelectValues(thisInput,picklistValues,config,valueDiv);
				});
			}
			if(value == '' || value == null)
			{
				$(valueDiv).text('click here to add values');
			}
		}
		else
		{
			thisInput.dataProvider(picklistValues);
			thisInput.selectedItems(selPicklistValues);
		}

		$(thisInput).attr('isRequired',config.required);
		$(thisInput).attr('reqLabel',config.label);

		if(config.required)
		{
			$(thisInput).addClass('required');
		}
		else
		{
			$(thisInput).removeClass('required');
		}
	}

	function setMultiSelectValues(thisMultiSelect,picklistValues,config,valueDiv)
	{
		var selectedValueString = '';
		  			
		for(var x = 0;x < thisMultiSelect._options.selectedItems.length;x++)
		{
			var thisSelectedValue = thisMultiSelect._options.selectedItems[x].data;
			var hasSelectedValue = false;
			
			if(picklistValues.length>0)
			{	
				for(var i=0;i<picklistValues.length;i++)
				{
					var pl = picklistValues[i];
					if(pl == null)
					{
						hasSelectedValue = false;
					}	
					else if(pl.data == thisSelectedValue)
					{
						hasSelectedValue = true;
					}
				}
				
				if(!hasSelectedValue)
				{
					thisMultiSelect._options.selectedItems.splice(x,1);
				}
			}
			else
			{
				thisMultiSelect._options.selectedItems.length = 0;
			}
		}
            		
		for(var x = 0;x < thisMultiSelect._options.selectedItems.length;x++)
		{
			var thisSelectedValue =thisMultiSelect._options.selectedItems[x].data;
			var hasSelectedValue = false;
			for(var i=0;i<picklistValues.length;i++)
			{
				var pl = picklistValues[i].data;
				if(pl == thisSelectedValue)
				{
					hasSelectedValue = true;
				}
			}
			
			if(hasSelectedValue)
			{	
				selectedValueString +=thisSelectedValue + ';';
			}
		}
		
		if(selectedValueString.length == 0)
		{
			selectedValueString = null;
			$(valueDiv).text('click here to add values');
		}
		else
		{
			$(valueDiv).text(selectedValueString);
			$(valueDiv).attr('title',selectedValueString);
		}					
	
		unsavedChanges = true;
    }
    
    function SummaryCubeComponentContainer(_this,component,config,hasHeaders)
	{
		this.helpText = config.helpText

		if(this.helpText == null)
		{
			this.helpText = '';
		}

		this.disabled = (config.accessLevel == 'Read');
		
		this.elementId = config.fieldName + ':' + config.objectName;

		if(this.subSectionItem == null)
		{			
			this.subSectionItem = $('<div id="' + this.elementId + 'subSectionItem" class="componentContainer subSectionItem" style="margin: 10px 0px 10px 0"></div>').appendTo(component);
			
			if(!hasHeaders)
			{
				this.labelComponent =  $('<div title="' + this.helpText  + '" class="label"></div>').appendTo(this.subSectionItem);
				this.labelSpan = $('<div>' + config.label + '</div>').appendTo(this.labelComponent);
			}

			this.editContainer = $('<div  id="' + this.elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(this.subSectionItem);

			if(config.required == true)
			{	
				$('<span style="color:#ff0000">*</span>').appendTo(this.labelComponent);
				$(this.subSectionItem).addClass('required');
			}
		}
		
		return this;
	}

    // Helper functions begins

	function createButton(_this,button)
	{
		var component = _this[button.buttonContainer];	
		var foundValue = false;	
		var type = button.type;
		var objName = objectName;
		var fldName  = button.fieldName;
		var elementId = objName + ':' + fldName;
		var mergeVal;
		var urlTar = button.urlTarget;
		var urlPre = button.urlPrefix;
		var urlSuf = button.urlSuffix;
		var label  = button.label;
		var fullURL;

		if(button.buttonId != null)
		{
			elementId = button.buttonId;
		}

		if(label == null)
		{
			label = '';
		}

		if(urlTar == null)
		{
			urlTar = '';
		}

		if(urlPre == null)
		{
			urlPre = '';
		}

		if(urlSuf == null)
		{
			urlSuf = '';
		}
		
		fullURL = urlPre;

		if(objName != null && fldName != null)
		{
			var objVal = _this.subject;
			if(objVal != null)
			{
				mergeVal = objVal[fldName];
				fullURL = urlPre + mergeVal + urlSuf;
				foundValue = true;
			}
		}
		
		if(!foundValue && button.fieldNameValue != null)
		{
			fullURL = urlPre + button.fieldNameValue + urlSuf;
		}

		var thisButton = _this.buttonMap[elementId];

		if(thisButton == null)
		{
			thisButton = $('<button class="confirmButton" title="' + label + '">' + label + '</button>').appendTo(component);
			_this.buttonMap[elementId] = thisButton;

			if(button.svgIcon != null)
			{
				$(thisButton).append(button.svgIcon);
				$(thisButton).attr('class','genericButton');
			}
			else if(button.classNames != null)
			{
				$(thisButton).attr('class',button.classNames);
			}

			switch(type.toUpperCase())
			{
				case "EVENT" :
					$(thisButton).click(function()
					{
						var action = $(this).attr('action');
	
						if(action != null && action.length > 0)
						{	
							ACE.CustomEventDispatcher.trigger(action);
						}					
					});
					break;
				case "URL" :
					$(thisButton).click(function()
					{
						var fullURL = $(thisButton).attr('fullURL');
						navigate(fullURL);
					});
					break;
				//Need to put in a special case for the back button because of the Cordova plugin override for mobile
				case "BACK" :
					$(thisButton).click(function()
					{
						window.close();
					});
					break;
			}

		}
			
		$(thisButton).attr('mergeVal',mergeVal);
		$(thisButton).attr('action',button.action);
		
		if(fullURL != null)
		{
			$(thisButton).attr('fullURL',fullURL);
		}
		
		$(thisButton).attr('target',urlTar);				
	}

	function navigate(url)
	{
		if(unsavedChanges)
		{
			ACEConfirm.show
			(
				'<p>' + unsavedWarningMessage + '</p>',
				"Warning",
				[
					"OK::confirmButton","Cancel::cancelButton"
				],
				function(result)
				{	
					if(result == 'OK')
					{
						window.location.href = url;
					}
				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
			);
		}
		else
		{
			window.location.href = url;
		}
	}
	
    function getObjectDescribeResult(config)
	{
		var objectDescResult;
		for(var x=0;x<describeSObjectResults.length;x++)
		{
			var objDesc = describeSObjectResults[x];
			if(objDesc.name == config.objectName)
			{
				objectDescResult = objDesc;
				break;
			}
		}

		return objectDescResult;
	}

	function getFieldDescribeResult(config,fieldName)
	{
		var objectDescResult = getObjectDescribeResult(config);
		for(var i = 0; i < objectDescResult.fields.length; i++) 
        {
            var fieldDesc = objectDescResult.fields[i];
            var fieldDescName = fieldDesc.name;
     
            if(fieldDescName.toLowerCase() == fieldName.toLowerCase())
            {
				return fieldDesc;
            }
		}
	}

	function getPicklistValuesForDependantPicklist(_this,config,isMultiSelect)
	{
		var picklistValues = []; 
		var value = config.record[config.fieldName];
		var controllingFieldValue = config.record[config.controllingField.name];
		var selectedIndex;

		var objectDescResult = getObjectDescribeResult(config);
		
		for(var i = 0; i < objectDescResult.fields.length; i++) 
        {
            var fieldDesc = objectDescResult.fields[i];
            var fieldName = fieldDesc.name;
     
            if(fieldName.toLowerCase() == config.fieldName.toLowerCase())
            {
				config.validForPicklist = fieldDesc.picklistValues;
				break;
            }
		}
		
		if(config.controllingField.name != null)
		{
			controllingFieldValue = _this.picklistValueTracker[config.controllingField.name + ':' + config.objectName + ':' + config.recId];
			var isValueInPicklistMetadata = false;			
			var isParentValueValid = false;
			
			for(var t=0;t<config.validForPicklist.length;t++)
			{
				if(config.validForPicklist[t].value == value)
				{
					isValueInPicklistMetadata = true;
				}
			}
			
			if(controllingFieldValue == null)
			{
				selectedIndex = 0;
			}

			if(config.controllingField.type == 'boolean')
			{
				if(controllingFieldValue == 'true')
				{
					selectedIndex = 1;
				}
				else
				{
					selectedIndex = 0;
				}
			}
			else if(config.controllingField.type == 'picklist' && controllingFieldValue != '' && selectedIndex == null)
			{
				for(var e=0;e<config.controllingField.picklistValues.length;e++)
				{
					if(config.controllingField.picklistValues[e].value == controllingFieldValue)
					{
						selectedIndex = e;
						isParentValueValid = true;
					}
				}
			}
			
			if(isMultiSelect == false)
			{
				picklistValues.push({data : '', label : 'Please select an option', defaultSelected : (value == '')});
				if(!isValueInPicklistMetadata && value != null)
				{
					picklistValues.push({data : value, label : value, defaultSelected : true});
				}		
			}
						
			if(config.validForPicklist !== null && config.validForPicklist.length !== null)
			{		
				for(var t=0;t<config.validForPicklist.length;t++)
				{
					if(isDependentValueValidForSelectedParent(selectedIndex, config.validForPicklist[t]) == true)
					{
						var thisPicklistValue = config.validForPicklist[t];
						var isSelected = false;
						
						if(isMultiSelect == true && value != null && value.length != '')
						{
							var selVals = value.split(';');
							
							for(var i=0;i<selVals.length;i++)
							{
								var fldVal = selVals[i];
					
								if(fldVal == thisPicklistValue.value)
								{
									isSelected =true;
									break;
								}
							}
						}	
						else if(value != null)
						{
							isSelected = (thisPicklistValue.value == value);
						}
						
						picklistValues.push({data : thisPicklistValue.value, label : thisPicklistValue.label, defaultSelected : isSelected});
					}
				}
			}
		}
		
		return picklistValues;
	}

	function isDependentValueValidForSelectedParent(parentSelIndex, depValue)
	{
	    var b64 = new sforce.Base64Binary("");
	    var validFor = depValue.validFor;
	    var decodedVF = b64.decode(validFor);
	    var bits = decodedVF.charCodeAt(parentSelIndex >> 3);
	    if((bits & (0x80 >> (parentSelIndex%8))) != 0)
	    {
	        return true;
	    }
	    return false;
	}

	function Picklist(){}
	
	if (!Picklist.prototype.watchPicklistValue) {
		Object.defineProperty(Picklist.prototype, "watchPicklistValue", {
			  enumerable: false
			, configurable: true
			, writable: false
			, value: function (prop, handler) 
			{
				var oldval = this[prop], getter = function (){return oldval;}, setter = function (newval) 
				{
					if (oldval !== newval) 
					{						
						oldval = newval;
						handler.call(this, prop, oldval, newval);
					}
					else { return false }
				}
				;
				
				if (delete this[prop]) { // can't watch constants
					Object.defineProperty(this, prop, {
						  get: getter
						, set: setter
						, enumerable: true
						, configurable: true
					});
				}
			}
		});
	}

	if (!Picklist.prototype.unwatchPicklistValue) {
		Object.defineProperty(Picklist.prototype, "unwatchPicklistValue", {
			  enumerable: false
			, configurable: true
			, writable: false
			, value: function (prop) 
			{
				var val = this[prop];
				delete this[prop]; // remove accessors
				this[prop] = val;
			}
		});
	}


	function SectionVisibility(){}

	if (!SectionVisibility.prototype.watchValue) 
	{
		Object.defineProperty(SectionVisibility.prototype, "watchValue", 
		{
			  enumerable: false
			, configurable: true
			, writable: false
			, value: function (prop, handler) 
			{
				var oldval = this[prop], getter = function (){return oldval;}, setter = function (newval) 
				{
					if (oldval !== newval) 
					{						
						oldval = newval;
						handler.call(this, prop, oldval, newval);
					}
					else { return false }
				};
				
				if (delete this[prop]) { // can't watch constants
					Object.defineProperty(this, prop, 
					{
						  get: getter
						, set: setter
						, enumerable: true
						, configurable: true
					});
				}
			}
		});
	}

	function getElementId(config)
	{
		if(config.fieldName == null || config.objectName == null)
		{
			return null;
		}

		var recId = 'isNew';

		if(config.record != null && config.record.Id != null)
		{
			recId = config.record.Id;
		}

		config.recId = recId;

		return config.fieldName + ':' + config.objectName + ':' + recId;
	}

	function getRowId(config)
	{
		return config.relationshipName + ':' + config.sObjectName + ':' + config.record.Id;
	}

	function getDateTimeValue(config)
	{
		var value = getValue(config);

		if($.isNumeric(value) && value > 0)
		{
			if(config.handler == 'DateTime')
			{
				value = new Date(value);
			}
			else
			{
				var offsetValue = new Date().getTimezoneOffset();
				var millisecondOffsetValue = offsetValue * 60000;
				value = new Date(value + millisecondOffsetValue);
			}	
		}
		else if($.type(value) == 'string' && value != '')
		{	
			var splitChar = 'T';
			if(value.indexOf('T') === -1)
			{
				splitChar = ' ';
			}
				
			var datevalue = value.split(splitChar)[0];
			var time = value.split(splitChar)[1];
			var thisYear = datevalue.split('-')[0];
			var thisMonth = datevalue.split('-')[1];
			var thisDay = datevalue.split('-')[2];
			var thisHour = 0;
			var thisMinute = 0;
			
			if(time != null)
			{
				thisHour = time.split(':')[0];
				thisMinute = time.split(':')[1];
			}
			
			value = new Date(thisYear, thisMonth -1, thisDay,thisHour,thisMinute);
		}
		else
		{
			value = null;
		}

		return value;
	}

	function getValue(config)
	{
        var thisData;

        if((config.record == null || config.record.isNew == true) && config.fieldNameData != null)
        {
			if(config.fieldNameLabel != null)
			{
				thisData = config.fieldNameLabel;
			}
			else if(config.fieldNameData != null)
			{
				thisData = config.fieldNameData;
			}            
        }
        else
        {
			thisData = config.record[config.fieldName];

			if(thisData == null)
			{
				var thisLinkFields = config.fieldName.split('.');
				thisData = config.record;
				
				for(var x=0;x<thisLinkFields.length;x++)
				{
					if(thisData != null)
					{
						thisData = thisData[thisLinkFields[x]];
					}
				}
				
				if(thisData == null)
				{
					thisData = '';
				}
			}
        }

		return thisData;
    }
    
    $.extend(true, window,
    {
        "DynamicPage": DynamicPage
    });

	new DynamicPage();

	var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
		isMobile = true;
	}	
//# sourceURL=DynamicPage.js	
})(jQuery);