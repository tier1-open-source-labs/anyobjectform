public without sharing class RestPicklistReturn
{
    public controllerValues controllerValues;
    public defaultValue defaultValue;
    public String eTag; 
    public String url;  
    public values[] values;
    
    public class controllerValues {}
    public class defaultValue  {}
    
    public class values
    {
        public attributes attributes;
        public String label;    //Web
        //public validFor[] validFor;
        public String value;    //Web
    }
    
    public class attributes {}
    
    public class validFor{}
    
    public static RestPicklistReturn parse(String json)
    {
        return (RestPicklistReturn) System.JSON.deserialize(json, RestPicklistReturn.class);
    }
}